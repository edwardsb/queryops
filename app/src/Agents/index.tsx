import React from 'react';

import { Agent } from './types';

import styles from './styles.module.css';

type ListProps = {
  allAgents: Agent[];
  onDeselect: ListItemProps['onDeselect'];
  onSelect: ListItemProps['onSelect'];
  selectedAgents: string[];
};

type ListItemProps = {
  agent: Agent;
  isSelected: boolean;
  onDeselect: (agent: Agent) => void;
  onSelect: (agent: Agent) => void;
};

function AgentListItem(props: ListItemProps) {
  const handleToggle = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.checked) {
      props.onSelect(props.agent);
    } else {
      props.onDeselect(props.agent);
    }
  };

  const id = `agent-${props.agent.id}`;
  return (
    <li>
      <input
        checked={props.isSelected}
        id={id}
        onChange={handleToggle}
        type="checkbox"
      />
      <label htmlFor={id}>{props.agent.name}</label>
    </li>
  );
}

function Agents(props: ListProps) {
  return (
    <ul className={styles.agents}>
      {props.allAgents.map(agent => (
        <AgentListItem
          agent={agent}
          key={agent.id}
          isSelected={props.selectedAgents.includes(agent.id)}
          onDeselect={props.onDeselect}
          onSelect={props.onSelect}
        />
      ))}
    </ul>
  );
}

export { Agents };
