class MockSocket {
  onmessage: any;
  private timer: any;
  private url: string;

  constructor(url: string) {
    this.url = url;
  }

  addEventListener = (type: string, handler: (e: MessageEvent) => void) => {
    if (type === 'message') {
      this.onmessage = handler;
    } else if (type === 'open') {
      handler({} as any);
    }
  }

  close = () => {
    // Do nothing
  }

  send = (data: string | ArrayBuffer | SharedArrayBuffer | Blob | ArrayBufferView) => {
    console.log('SENT TO SOCKET');
    let id = 0;
    this.timer = setInterval(() => {
      this.onmessage({
        data: {
          id: `${id++}`,
          from: 'agent1',
          payload: {
            cmdline: '/sbin/init splash',
            cwd: '/',
            disk_bytes_read: '467905024',
            disk_bytes_written: '350306304',
            egid: '0',
            euid: '0',
            gid: '0',
            name: 'systemd',
            nice: '0',
            on_disk: '1',
            parent: '0',
            path: '/lib/systemd/systemd',
            pgroup: '1',
            pid: '1',
            resident_size: '11380000',
            root: '/',
            sgid: '0',
            start_time: '1588602598',
            state: 'S',
            suid: '0',
            system_time: '27890',
            threads: '1',
            total_size: '167448000',
            uid: '0',
            user_time: '37090',
            wired_size: ''
          },
          subject: 'foo',
          timestamp: `2020-01-01T09:${id < 10 ? '0' + id : id}:00-0400`
        }
      });

      if (id >= 60) {
        clearTimeout(this.timer);
        this.timer = null;
      }
    }, 1000);
  }
}

export { MockSocket };
