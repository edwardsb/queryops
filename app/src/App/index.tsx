import React, { useEffect, useState } from 'react';

import { QueryResult } from '../Query/types';

import { Agents } from '../Agents';
import { Query } from '../Query';
import { Results } from '../Results';

import { getColumnDefinitions } from '../Results/utils';

import * as service from '../service';

import { useSocket } from './useSocket';

import styles from './styles.module.css';
import { ColDef } from 'ag-grid-community';

type Agent = {
  id: string;
  name: string;
};

const ACCOUNT_ID = '123';
const CLIENT_ID = 'benfoo' + ;

function App() {
  const [allAgents, setAllAgents] = useState<Agent[]>([]);
  const [allColumns, setAllColumns] = useState<ColDef[]>([]);
  const [queryResults, setQueryResults] = useState<QueryResult[]>([]);
  const [selectedAgents, setSelectedAgents] = useState<string[]>([]);
  const [visibleColumns, setVisibleColumns] = useState<ColDef[]>([]);

  const { lastError, lastMessage } = useSocket(`${process.env.REACT_APP_WS_HOST}/api/v1/account/${ACCOUNT_ID}/client/${CLIENT_ID}/websocket`);

  useEffect(() => {
    if (lastMessage) {
      switch (lastMessage.subject) {
        case 'subscribed.agents': {
          // TODO: Show real agent names, right now show fake agent names
          const agents = lastMessage.payload.map((agent, index) => ({ id: agent, name: `Agent ${index}` }));
          setAllAgents(agents);
          setSelectedAgents(lastMessage.payload);
          break;
        }
        case 'osquery': {
          const results: QueryResult[] = [...queryResults, lastMessage];
          setQueryResults(results);

          // Figure out the columns to show
          const cols = getColumnDefinitions(results);
          setAllColumns(cols);
          setVisibleColumns(cols);
        }
      }
    }
  }, [lastMessage]);

  const handleDeselectAgent = (agent: Agent) => {
    setSelectedAgents(selectedAgents.filter(id => id !== agent.id));
    
    // TODO: Maybe run the query automatically?
  };

  const handleSelectAgent = (agent: Agent) => {
    setSelectedAgents([...selectedAgents, agent.id]);

    // TODO: Maybe run the query automatically?
  };

  const handleSubmitQuery = async (query: string) => {
    console.log('QUERY', query);

    setQueryResults([]);

    try {
      await service.sendQuery(ACCOUNT_ID, CLIENT_ID, query, selectedAgents)
    } catch (err) {
      console.log('RESPONSE ERROR', err);
    }
  };

  return (
    <main className={styles.app}>
      <section className={styles.sidebar}>
        <Agents
          allAgents={allAgents}
          onDeselect={handleDeselectAgent}
          onSelect={handleSelectAgent}
          selectedAgents={selectedAgents}
        />
      </section>
      <section className={styles.payload}>
        <Query onSubmit={handleSubmitQuery} />
        <Results
          allColumns={allColumns}
          data={queryResults}
          visibleColumns={visibleColumns}
        />
      </section>
    </main>
  );
}

export default App;
