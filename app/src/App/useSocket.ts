import { useEffect, useRef, useState } from 'react';

import { MockSocket } from './MockSocket';

export type Message = {
  id: string;
  subject: 'osquery' | 'subscribed.agents';
  payload: string[];
  timestamp: string;
};

function useSocket(url: string) {
  const [lastError, setLastError] = useState<any>(null);
  const [lastMessage, setLastMessage] = useState<Message | null>(null);
  const socket = useRef<WebSocket | MockSocket | null>(null);

  useEffect(() => {
    const ws = new WebSocket(url);
    // const ws = new MockSocket(url);
    socket.current = ws;

    ws.addEventListener('error', err => {
      console.log('SOCKET ERROR', err);
      setLastError(err);
    });

    ws.addEventListener('message', e => {
      console.log('SOCKET MESSAGE', e);
      setLastMessage(JSON.parse(e.data));
    });

    ws.addEventListener('open', e => {
      console.log('SOCKET OPENED', e);
    });

    return () => {
      ws.close();
    };
  }, []);

  return {
    lastError,
    lastMessage
  };
}

export { useSocket };
