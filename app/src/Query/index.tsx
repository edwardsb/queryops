import React, { useState } from 'react';
import Editor from 'react-simple-code-editor';
import { highlight, languages } from 'prismjs';
import 'prismjs/components/prism-sql';

import './vs-theme.css';
import styles from './styles.module.css';

type Props = {
  onSubmit: (query: string) => void;
};

function Query(props: Props) {
  const [query, setQuery] =  useState('');

  const handleRun = () => {
    props.onSubmit(query);
  };

  const handleShiftClick = (e: React.KeyboardEvent<HTMLElement>) => {
    if (e.key.toUpperCase() === 'ENTER' && e.shiftKey) {
      props.onSubmit(query);
    }
  };

  return (
    <div className={styles.container}>
      <div className={`${styles.query}`}>
        <Editor
          autoFocus={true}
          highlight={code => highlight(code, languages.sql, 'sql')}
          onKeyDown={handleShiftClick}
          onValueChange={code => setQuery(code)}
          placeholder="Enter a query then press Run"
          value={query}
        />
      </div>
      <button type="button" onClick={handleRun}>Run</button>
      <br />
    </div>
  );
}

export { Query };
