import { Message } from '../App/useSocket';

export type QueryResult = Message & {
  payload: {
    [key: string]: any;
  };
};
