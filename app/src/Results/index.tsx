import React from 'react';

import { AgGridReact as AgGridTable } from 'ag-grid-react';
import { ColDef } from 'ag-grid-community';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine-dark.css';

import { QueryResult } from '../Query/types';

type Props = {
  allColumns: ColDef[];
  data: QueryResult[];
  visibleColumns: ColDef[];
};

function Results(props: Props) {
  return (
    <div style={{ flex: 1 }} className="ag-theme-alpine-dark">
      <AgGridTable
        columnDefs={props.visibleColumns || []}
        rowData={props.data}
        rowHeight={45}
      />
    </div>
  );
}

export { Results };
