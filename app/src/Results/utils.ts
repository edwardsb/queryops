import { QueryResult } from '../Query/types';

function getColumnDefinitions(data: QueryResult[]) {
  const cols: Array<{ headerName: string; field: string; }> = [];
  data.reduce((arr, result) => {
    const keys = Object.keys(result.payload);
    keys.forEach(col => {
      if (!arr.includes(col)) {
        arr.push(col);
        cols.push({ headerName: col, field: `payload.${col}` });
      }
    });
    return arr;
  }, [] as string[]);
  return cols;
}

export {
  getColumnDefinitions
};
