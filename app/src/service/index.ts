// declare var process : {
//     env: {
//         API_HOST: string
//     }
// }

function sendQuery(accountId: string, clientId: string, query: string, agents: string[]) {
    const headers = new Headers();
    headers.append('Content-type', 'application/json');

    return fetch(`${process.env.REACT_APP_API_HOST}/api/v1/account/${accountId}/client/${clientId}`, {
        headers,
        method: 'POST',
        body: JSON.stringify({
            message: {
                id: 'some-message-id-0',
                from: clientId,
                payload: {
                    query
                },
                subject: 'osquery'
            },
            recipients: agents
        })
    });
}

export {
    sendQuery
};
