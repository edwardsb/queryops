FROM golang:1.16-stretch as build

WORKDIR /src

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .
RUN make build

FROM ubuntu:16.04

ENV AGENT_WS_PORT=443
ENV AGENT_WS_HOST=app.queryops.com

RUN apt-get update \
  && apt-get install -y software-properties-common \
  && apt-get install apt-transport-https

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 1484120AC4E9F8A1A577AEEE97A80C63C9D8B80B \
 && add-apt-repository 'deb [arch=amd64] https://pkg.osquery.io/deb deb main' \
 && apt-get update \
 && apt-get install osquery

COPY osquery.example.conf /etc/osquery/osquery.conf
COPY osquery.example.flags /etc/osquery/osquery.flags
COPY scripts/agent_entrypoint.sh /opt/app/agent_entrypoint.sh
COPY --from=build /src/queryops /opt/app/queryops
RUN chmod +x -R /opt/app/

ENTRYPOINT ["/opt/app/agent_entrypoint.sh"]
CMD ["/opt/app/queryops", "agent"]
