package agent

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/osquery/osquery-go"
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules/messaging"
)

type QueryWorkerConfig struct {
	SocketPath string
	Timeout    time.Duration
}

type OSQueryResult struct {
	foo string
}

type QueryWorker struct {
	Inbox          <-chan *messaging.Message
	Outbox         chan<- *messaging.Envelope
	ExtensionEvent chan OSQueryResult
	done           chan struct{}
	shutdown       chan struct{}
	config         QueryWorkerConfig
	client         *osquery.ExtensionManagerClient
	logger         logging.Interface
}

func NewQueryWorker(inbox <-chan *messaging.Message, outbox chan<- *messaging.Envelope, config QueryWorkerConfig, log logging.Interface) *QueryWorker {
	return &QueryWorker{
		Inbox:    inbox,
		Outbox:   outbox,
		done:     make(chan struct{}),
		shutdown: make(chan struct{}),
		config:   config,
		logger:   log.WithTag("client.query_worker"),
	}
}

func (w *QueryWorker) Open() error {
	client, err := osquery.NewClient(w.config.SocketPath, w.config.Timeout)
	if err != nil {
		return fmt.Errorf("failed to connect to osquery extension socket %w", err)
	}
	w.client = client
	go w.run()
	return nil
}

func (w *QueryWorker) Close() error {
	close(w.done)

	select {
	case <-w.shutdown:
		break
	case <-time.After(3 * time.Second):
		w.logger.Warn("failed shutdown")
		break
	}
	w.logger.Info("closed")
	return nil
}

func (w *QueryWorker) run() {
	go w.processMessages()
	//
	// wait till termination
	<-w.done
	// place holder for other tasks needed
	//

	// shut down dependencies
	w.client.Close()

	w.shutdown <- struct{}{}
}

func (w *QueryWorker) processMessages() {
	for {
		select {
		case event := <-w.ExtensionEvent:
			go w.handelEvent(event)
		case message := <-w.Inbox:
			go w.handleMessage(message)
		case <-w.done:
			return
		}
	}
}

func (w *QueryWorker) handelEvent(event OSQueryResult) {

}

func (w *QueryWorker) handleMessage(message *messaging.Message) {
	switch message.Subject {
	case "osquery":
		w.processQuery(message)
	case "echo":
		w.Outbox <- &messaging.Envelope{
			Recipients: []string{message.From},
			Message:    message,
		}
	default:
		return
	}

}

func (w *QueryWorker) processQuery(message *messaging.Message) {
	if message == nil {
		return
	}
	plog := w.logger.With("message.subject", message.Subject)
	plog.Info("message received!")
	type queryCommandPayload struct {
		Query string `json:"query"`
	}
	var qc queryCommandPayload
	err := json.Unmarshal(message.Payload, &qc)
	if err != nil {
		plog.Error("failed to parse query command")
		w.Outbox <- &messaging.Envelope{
			Recipients: []string{message.From},
			Message:    &messaging.Message{
				ID:        message.ID,
				Subject:   "",
				Payload:   w.newErrorPayload(err),
				Timestamp: time.Now(),
			},
		}
	}
	plog = plog.With("query", qc.Query)
	rows, err := w.client.QueryRows(qc.Query)
	if err != nil {
		plog.WithError(err).Error("client query error")
		w.Outbox <- &messaging.Envelope{
			Recipients: []string{message.From},
			Message:    &messaging.Message{
				ID:        message.ID,
				Subject:   "error",
				Payload:   w.newErrorPayload(err),
				Timestamp: time.Now(),
			},
		}
	}
	for _,r := range rows {
		buf, err := json.Marshal(r)
		if err != nil {
			plog.WithError(err).Error("failed to serialize row")
			continue
		}
		w.Outbox <- &messaging.Envelope{
			Recipients: []string{message.From},
			Message:    &messaging.Message{
				ID:        message.ID,
				From:      "agent1",
				Subject:   message.Subject,
				Payload:   buf,
				Timestamp: time.Now(),
			},
		}
	}
}

func (w *QueryWorker) newErrorPayload(err error) json.RawMessage {
	type errorMessage struct {
		error string
	}
	msg := errorMessage{error: err.Error()}
	buf, err := json.Marshal(msg)
	if err != nil {
		w.logger.WithError(err).Error("failed to marshal error")
		return []byte{}
	}
	return buf
}
