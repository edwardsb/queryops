package agent

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"time"

	"github.com/cenkalti/backoff"
	"github.com/gorilla/websocket"
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules/messaging"
)

type Config struct {
	Host string
	Port int
}

func NewClient(logger logging.Interface, config Config) *client {
	return &client{
		logger:   logger.WithTag("queryd.wsclient"),
		config:   config,
		done:     make(chan struct{}),
		shutdown: make(chan struct{}),
		Inbox:    make(chan *messaging.Message, 256),
		Outbox:   make(chan *messaging.Envelope, 256),
	}
}

type client struct {
	done     chan struct{}
	shutdown chan struct{}
	config   Config
	logger   logging.Interface

	Inbox  chan *messaging.Message
	Outbox chan *messaging.Envelope
}

func (c *client) Open() error {
	go c.run()
	return nil
}

func (c *client) Close() error {
	close(c.done)
	<-c.shutdown
	return nil
}

func (c *client) run() {
	var conn *websocket.Conn = nil

	defer func() {
		defer func() { c.shutdown <- struct{}{} }()
		if conn == nil {
			return
		}

		c.logger.Info("closing websocket")
		err := conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		if err != nil {
			c.logger.WithError(err).Error("close connection error")
			return
		}
		<-time.After(time.Second)
		conn.Close()
	}()

	for {
		conn = nil
		op := func() error {
			var err error
			conn, err = c.connect()

			if err != nil {
				return err
			}

			return nil
		}

		ticker := backoff.NewTicker(backoff.NewExponentialBackOff())

		// Ticks will continue to arrive when the previous operation is still running,
		// so operations that take a while to fail could run in quick succession.
		for range ticker.C {
			if err := op(); err != nil {
				c.logger.WithError(err).Error("failed to connect")
				continue
			}

			ticker.Stop()
			break
		}

		rpch := c.readPump(conn)
		wpch := c.writePump(conn)
		select {
		case <-rpch:
			break
		case <-wpch:
			break
		case <-c.done:
			return
		}

		err := conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		if err != nil {
			c.logger.WithError(err).Error("close connection error")
			continue
		}
		<-time.After(time.Second)
		conn.Close()
	}
}

func (c *client) readPump(conn *websocket.Conn) chan error {
	errChan := make(chan error)

	go func(ch chan error) {
		for {
			var message messaging.Message
			var err error = nil
			select {
			case <-c.done:
				ch <- nil
			default:
				err = conn.ReadJSON(&message)
				if err != nil {
					ch <- err
					return
				}
				c.Inbox <- &message
			}
		}
	}(errChan)

	return errChan
}

func (c *client) writePump(conn *websocket.Conn) chan error {
	errChan := make(chan error)

	go func(ch chan error) {
		for {
			select {
			case envelope := <-c.Outbox:
				err := conn.WriteJSON(envelope)
				if err != nil {
					ch <- err
					return
				}
			case <-c.done:
				ch <- nil
			}
		}
	}(errChan)

	return errChan
}

func (c *client) connect() (*websocket.Conn, error) {
	var scheme = "ws"
	if c.config.Port == 0 || c.config.Port == 443 {
		scheme = "wss"
	}
	var host = c.config.Host
	if c.config.Port != 0 {
		host = fmt.Sprintf("%s:%d", c.config.Host, c.config.Port)
	}

	target := url.URL{Scheme: scheme, Host: host, Path: fmt.Sprintf("/api/v1/account/123/agent/smith_%d/websocket", rand.Int())}
	hdr := http.Header{}
	c.logger.With("target", target.String()).Info("Connecting to queryops agent endpoint")

	ws, _, err := websocket.DefaultDialer.Dial(target.String(), hdr)
	if err != nil {
		c.logger.WithError(err).Error("failed to dial server")
		return nil, err
	}
	c.logger.With("target", target.String()).Info("connection success")

	return ws, nil
}

