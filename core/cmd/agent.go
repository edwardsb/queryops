package cmd

import (
	"gitlab.com/edwardsb/queryops/core/config"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/edwardsb/queryops/core/agent"
	"gitlab.com/edwardsb/queryops/core/logging"

	"github.com/spf13/cobra"
)

// agentCmd represents the agent command
var agentCmd = &cobra.Command{
	Use:   "agent",
	Short: "A brief description of your command",
	Run: func(cmd *cobra.Command, args []string) {
		logger := logging.New().WithTag("agent")
		cfg := config.NewQueryOpsConfiguration()

		wsclient := agent.NewClient(logger, agent.Config{
			Host: cfg.AgentWSHost(),
			Port: cfg.AgentWSPort(),
		})

		queryWorker := agent.NewQueryWorker(wsclient.Inbox, wsclient.Outbox, agent.QueryWorkerConfig{
			SocketPath: cfg.AgentSocket(),
			Timeout:    time.Second * 5,
		}, logger)

		err := wsclient.Open()
		if err != nil {
			logger.WithError(err).Panic("failed to open ws")
		}
		err = queryWorker.Open()
		if err != nil {
			logger.WithError(err).Panic("failed to open query worker")
		}
		//
		// wait for shutdown
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

		select {
		case <-sigChan:
			logger.Info("shutdown signal received")
			queryWorker.Close()
			wsclient.Close()
		}
	},
}

func init() {
	rootCmd.AddCommand(agentCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// agentCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// agentCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
