package cmd

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-redis/redis/v7"
	"github.com/spf13/cobra"
	"gitlab.com/edwardsb/queryops/core/config"
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules"
	"gitlab.com/edwardsb/queryops/core/modules/http"
	"gitlab.com/edwardsb/queryops/core/modules/http/api"
	"gitlab.com/edwardsb/queryops/core/modules/pubsub"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		logger := logging.New().WithTag("websocket-server")
		cfg := config.NewQueryOpsConfiguration()

		logger.Info("starting websocket server")

		components := map[string]modules.Module{}
		//
		// pubsub
		client := redis.NewClient(&redis.Options{
			Addr: fmt.Sprintf("%s:%d", cfg.PubsubHost(), cfg.PubsubPort()),
		})

		aps := pubsub.NewPubsub(pubsub.SetChannelPrefix("na"), pubsub.SetClient(client), pubsub.Subscribe(cfg.PubsubTopic()))
		components["agent-pubsub"] = aps
		cps := pubsub.NewPubsub(pubsub.SetChannelPrefix("nc"), pubsub.SetClient(client), pubsub.Subscribe(cfg.PubsubTopic()))
		components["client-pubsub"] = cps
		//
		// ws api http router
		wssv1 := api.NewWebsocketRouterV1(api.SetAgentPubsub(components["agent-pubsub"].(pubsub.Interface)), api.SetClientPubsub(components["client-pubsub"].(pubsub.Interface)), api.SetLogger(logger.WithTag("api.wss_v1")))
		//
		// websocket server
		components["websocket-controller"] = http.NewHTTP([]modules.HTTPModule{wssv1})
		//
		// load component modules
		for k, component := range components {
			if err := component.Open(); err != nil {
				logger.WithError(err).With("component", k).Panic("failed loading")
			}
			logger.With("component", k).Info("loaded")
		}
		//
		// wait for shutdown
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

		select {
		case <-sigChan:
			logger.Info("shutdown signal received")
			for k, component := range components {
				if err := component.Close(); err != nil {
					logger.WithError(err).With("component", k).Warn("failed unloading")
				}
				logger.With("component", k).Info("unloaded")
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
