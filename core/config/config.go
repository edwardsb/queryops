package config

import (
	"strings"
	"sync"
	"time"

	"github.com/spf13/viper"
)

type ConfigurationProvider interface {
	//
	// server
	UseSSL() bool
	SSLCert() string
	SSLKey() string
	ServerPort() int
	//
	// APM
	ServiceName() string
	APMEndpointURL() string
	APMAccessToken() string
	//
	// pubsub
	PubsubTopic() string
	PubsubHost() string
	PubsubPort() int

	//
	// agent
	AgentWSHost() string
	AgentWSPort() int
	AgentSocket() string
}

func NewQueryOpsConfiguration() ConfigurationProvider {
	cfg := &QueryOpsConfiguration{
		mux: sync.RWMutex{},
		v:   newDefaultQueryOpsConfigurationViper(),
	}

	return cfg
}

const (
	httpServerPort = "http.server-port"
	httpSSLUse     = "http.ssl.use"
	httpSSLCert    = "http.ssl.cert"
	httpSSLKey     = "http.ssl.key"

	apmServiceName  = "apm.service.name"
	apmServiceURL   = "apm.service.url"
	apmServiceToken = "apm.service.token"

	pubsubTopic = "pubsub.topic"
	pubsubHost  = "pubsub.host"
	pubsubPort  = "pubsub.port"

	agent_ws_host = "agent.ws.host"
	agent_ws_port = "agent.ws.port"
	agent_socket  = "agent.socket"
)

func newDefaultQueryOpsConfigurationViper() *viper.Viper {
	v := viper.New()
	v.SetDefault(httpServerPort, 4444)
	v.SetDefault(httpSSLUse, false)
	v.SetDefault(httpSSLCert, "")
	v.SetDefault(httpSSLKey, "")

	v.SetDefault(apmServiceName, "queryops-api")
	v.SetDefault(apmServiceURL, "localhost:9800")
	v.SetDefault(apmServiceToken, "")

	v.SetDefault(pubsubTopic, "queryops")
	v.SetDefault(pubsubHost, "localhost")
	v.SetDefault(pubsubPort, 6379)

	v.SetDefault(agent_ws_host, "localhost")
	v.SetDefault(agent_ws_port, 4444)
	v.SetDefault(agent_socket, "/tmp/.queryops_soc")

	// set env prefix
	v.SetEnvPrefix("queryops")
	// replace - and . with _ in env variable lookup
	replacer := strings.NewReplacer("-", "_", ".", "_")
	v.SetEnvKeyReplacer(replacer)

	// Automatically override with defaults
	v.AutomaticEnv()
	return v
}

type QueryOpsConfiguration struct {
	mux sync.RWMutex
	v   *viper.Viper
}

//
// ConfigurationProvider
//

func (c *QueryOpsConfiguration) UseSSL() bool {
	return c.GetBool(httpSSLUse)
}

func (c *QueryOpsConfiguration) SSLCert() string {
	return c.GetString(httpSSLCert)
}

func (c *QueryOpsConfiguration) SSLKey() string {
	return c.GetString(httpSSLKey)
}

func (c *QueryOpsConfiguration) ServerPort() int {
	return c.GetInt(httpServerPort)
}

func (c *QueryOpsConfiguration) ServiceName() string {
	return c.GetString(apmServiceName)
}

func (c *QueryOpsConfiguration) APMEndpointURL() string {
	return c.GetString(apmServiceURL)
}

func (c *QueryOpsConfiguration) APMAccessToken() string {
	return c.GetString(apmServiceToken)
}

func (c *QueryOpsConfiguration) PubsubTopic() string {
	return c.GetString(pubsubTopic)
}

func (c *QueryOpsConfiguration) PubsubHost() string {
	return c.GetString(pubsubHost)
}

func (c *QueryOpsConfiguration) PubsubPort() int {
	return c.GetInt(pubsubPort)
}

func (c *QueryOpsConfiguration) AgentWSHost() string {
	return c.GetString(agent_ws_host)
}

func (c *QueryOpsConfiguration) AgentWSPort() int {
	return c.GetInt(agent_ws_port)
}

func (c *QueryOpsConfiguration) AgentSocket() string {
	return c.GetString(agent_socket)
}
//
// ConfigurationProvider
//

func (c *QueryOpsConfiguration) ConfigFileUsed() string {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.ConfigFileUsed()
}

func (c *QueryOpsConfiguration) Get(key string) interface{} {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.Get(key)
}

func (c *QueryOpsConfiguration) GetBool(key string) bool {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetBool(key)

}

func (c *QueryOpsConfiguration) GetDuration(key string) time.Duration {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetDuration(key)
}

func (c *QueryOpsConfiguration) GetFloat64(key string) float64 {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetFloat64(key)
}

func (c *QueryOpsConfiguration) GetInt(key string) int {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetInt(key)
}

func (c *QueryOpsConfiguration) GetInt64(key string) int64 {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetInt64(key)
}

func (c *QueryOpsConfiguration) GetSizeInBytes(key string) uint {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetSizeInBytes(key)
}

func (c *QueryOpsConfiguration) GetString(key string) string {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetString(key)
}

func (c *QueryOpsConfiguration) GetStringMap(key string) map[string]interface{} {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetStringMap(key)
}

func (c *QueryOpsConfiguration) GetStringMapString(key string) map[string]string {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetStringMapString(key)
}

func (c *QueryOpsConfiguration) GetStringMapStringSlice(key string) map[string][]string {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetStringMapStringSlice(key)
}

func (c *QueryOpsConfiguration) GetStringSlice(key string) []string {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetStringSlice(key)
}

func (c *QueryOpsConfiguration) GetTime(key string) time.Time {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.GetTime(key)
}

func (c *QueryOpsConfiguration) InConfig(key string) bool {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.InConfig(key)
}

func (c *QueryOpsConfiguration) IsSet(key string) bool {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.IsSet(key)
}

func (c *QueryOpsConfiguration) SetConfigFile(in string) {
	c.mux.Lock()
	defer c.mux.Unlock()
	c.v.SetConfigFile(in)
}

func (c *QueryOpsConfiguration) ReadInConfig() error {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.v.ReadInConfig()
}

func (c *QueryOpsConfiguration) Set(key string, value interface{}) {
	c.mux.Lock()
	defer c.mux.Unlock()
	c.v.Set(key, value)
}
