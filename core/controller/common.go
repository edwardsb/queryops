package controller

import (
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules/pubsub"
)

type pubsuber interface {
	AgentPubsub(ps pubsub.Interface)
	ClientPubsub(ps pubsub.Interface)
}

func SetAgentPubsub(ps pubsub.Interface) func(interface{}) {
	return func(i interface{}) {
		if c, ok := i.(pubsuber); ok {
			c.AgentPubsub(ps)
		}
	}
}

func SetClientPubsub(ps pubsub.Interface) func(interface{}) {
	return func(i interface{}) {
		if c, ok := i.(pubsuber); ok {
			c.ClientPubsub(ps)
		}
	}
}

type logger interface {
	Logger(log logging.Interface)
}

func SetLogger(log logging.Interface) func(interface{}) {
	return func(i interface{}) {
		if c, ok := i.(logger); ok {
			c.Logger(log)
		}
	}
}
