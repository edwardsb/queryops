package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/gorilla/websocket"
	"gitlab.com/edwardsb/queryops/core/logging"
	httpc "gitlab.com/edwardsb/queryops/core/modules/http"
	"gitlab.com/edwardsb/queryops/core/modules/messaging"
	"gitlab.com/edwardsb/queryops/core/modules/pubsub"
)

var _ httpc.Controller = (*queryopsController)(nil)

type queryopsController struct {
	agentPubsub  pubsub.Interface
	clientPubsub pubsub.Interface
	agentRouter  *messaging.MessageRouter
	clientRouter *messaging.MessageRouter
	logger       logging.Interface

	agentsMutex sync.RWMutex
	clientsMutex sync.RWMutex

	agents  map[string]map[string]interface{}
	clients map[string]map[string]interface{}
}

func NewQueryOpsController(options ...func(interface{})) httpc.Controller {
	ctrl := &queryopsController{
		agentPubsub:  pubsub.NewMemory(),
		clientPubsub: pubsub.NewMemory(),
		agentRouter:  messaging.NewMessageRouter(),
		clientRouter: messaging.NewMessageRouter(),
		logger:       logging.New(),
		agents:       make(map[string]map[string]interface{}),
		clients:      make(map[string]map[string]interface{}),
	}

	for _, f := range options {
		if f == nil {
			continue
		}

		f(ctrl)
	}

	ctrl.agentRouter.Open()
	ctrl.clientRouter.Open()

	go ctrl.onWebsocketInbox(context.Background())
	go ctrl.onAgentPubsubInbox(context.Background())
	go ctrl.onClientPubsubInbox(context.Background())

	return ctrl
}

func (c *queryopsController) Logger(logger logging.Interface) {
	c.logger = logger
}

func (c *queryopsController) AgentPubsub(ps pubsub.Interface) {
	c.agentPubsub = ps
}

func (c *queryopsController) ClientPubsub(ps pubsub.Interface) {
	c.clientPubsub = ps
}

func (c *queryopsController) MessageRouter(router *messaging.MessageRouter) {
	c.agentRouter = router
}

func (c *queryopsController) SetRoutes(router chi.Router) {
	router.Route("/account/{aid}/agent/{id}", func(r chi.Router) {
		//
		// websocket endpoint
		r.Get("/websocket", c.agentWebsocketHandler)
	})

	router.Route("/account/{aid}/client/{id}", func(r chi.Router) {
		//
		// websocket endpoint
		r.Get("/websocket", c.clientWebsocketHandler)

		// send query api
		r.Post("/", c.sendMessage)
	})
}

func (c *queryopsController) sendMessage(w http.ResponseWriter, r *http.Request) {
	b := &messaging.Envelope{}
	if err := render.Bind(r, b); err != nil {
		render.Status(r, http.StatusBadRequest)
		return
	}

	err := b.Send(context.Background(), c.agentPubsub)
	if err != nil {
		render.Status(r, http.StatusInternalServerError)
		return
	}
}

func (c *queryopsController) agentWebsocketHandler(w http.ResponseWriter, r *http.Request) {
	aid := chi.URLParam(r, "aid")
	id := chi.URLParam(r, "id")

	amap, ok := c.agents[aid]
	if !ok {
		amap = make(map[string]interface{})
		c.agents[aid] = amap
	}
	if _, ok := amap[id]; ok {
		// already connected (to this instance...)
		render.Status(r, http.StatusConflict)
		return
	}
	amap[id] = struct{}{}
	c.logger.With("aid", aid).With("id", id).Info("agent connecting")
	//
	// upgrade to websocket
	var upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		c.logger.WithError(err).Error("failed to upgrade websocket")
		return
	}

	client := messaging.NewClient(c.agentRouter, conn, []string{aid, id}, c.logger)
	c.agentRouter.Register(client)

	go client.WritePump()
	go client.ReadPump()

	go func() {
		//
		// subscribe to account specific channel
		c.logger.With("channel", aid).Info("subscribe to agent pubsub")
		c.agentPubsub.Subscribe(aid)
		//
		// subscribe to agent specific channel
		c.logger.With("channel", id).Info("subscribe to agent pubsub")
		c.agentPubsub.Subscribe(id)
		//
		// notify interested clients of agent joining
		envelope := messaging.Envelope{
			Recipients: []string{aid},
			Message: &messaging.Message{
				ID:        "0",
				From:      id,
				Subject:   "agent.subscribed",
				Payload:   nil,
				Timestamp: time.Now(),
			},
		}
		err := envelope.Send(context.Background(), c.clientPubsub)
		if err != nil {
			c.logger.WithError(err).Warn("failed to send subscribe notification")
		}
		//
		// wait for client disconnect
		<-client.Closed
		//
		// remove from agent tracking map
		delete(c.agents[aid], id)
		//
		// notify interested clients of agent leaving
		envelope = messaging.Envelope{
			Recipients: []string{aid},
			Message: &messaging.Message{
				ID:        "0",
				From:      id,
				Subject:   "agent.unsubscribed",
				Payload:   nil,
				Timestamp: time.Now(),
			},
		}
		err = envelope.Send(context.Background(), c.clientPubsub)
		if err != nil {
			c.logger.WithError(err).Warn("failed to send unsubscribe notification")
		}
		//
		// unsubscribe from agent specific channel
		c.logger.With("channel", aid).Info("unsubscribe from agent pubsub")
		c.agentPubsub.Unsubscribe(aid)
		//
		// unsubscribe from agent specific channel
		c.logger.With("channel", id).Info("unsubscribe from agent pubsub")
		c.agentPubsub.Unsubscribe(id)
	}()
}

// onPubSubInbox get messages off the client pubsub and process them, i.e. send
// to appropriate clients
func (c *queryopsController) onAgentPubsubInbox(ctx context.Context) {
	defer c.agentPubsub.Close()

	for {
		payload, err := c.agentPubsub.Receive(ctx)
		if err != nil {
			c.logger.WithError(err).Error("failed to receive message from agent pubsub")
			continue
		}

		go c.processAgentPubsubPayload(payload)
	}
}

// processPubSubPayload send the envelope to the client router where
// it will deliver to the addressed clients
func (c *queryopsController) processAgentPubsubPayload(payload []byte) {
	var envelope *messaging.Envelope

	if err := json.Unmarshal(payload, &envelope); err != nil {
		c.logger.WithError(err).Error("failed to deserialize envelope from agent pubsub")
		return
	}

	c.agentRouter.Send(envelope)
}

// onWebsocketInbox get and process envelopes sent by clients over
// websocket
func (c *queryopsController) onWebsocketInbox(ctx context.Context) {
	for {
		err := c.processReceivedEnvelope(ctx, c.agentRouter.Receive())
		if err != nil {
			c.logger.WithError(err).Error("failed to process received envelope")
		}
	}
}

func (c *queryopsController) processReceivedEnvelope(ctx context.Context, envelope *messaging.Envelope) error {
	c.logger.With("envelope", fmt.Sprintf("%+v", envelope)).Info("processing received envelope")
	err := envelope.Send(ctx, c.clientPubsub)
	if err != nil {
		return err
	}

	return nil
}

func (c *queryopsController) clientWebsocketHandler(w http.ResponseWriter, r *http.Request) {
	aid := chi.URLParam(r, "aid")
	id := chi.URLParam(r, "id")

	cmap, ok := c.clients[aid]
	if !ok {
		cmap = make(map[string]interface{})
		c.clients[aid] = cmap
	}
	if _, ok := cmap[id]; ok {
		// already connected (to this instance...)
		render.Status(r, http.StatusConflict)
		return
	}
	cmap[id] = struct{}{}
	c.logger.With("aid", aid).With("id", id).Info("client connecting")
	//
	// upgrade to websocket
	var upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		c.logger.WithError(err).Error("failed to upgrade websocket")
		return
	}

	client := messaging.NewClient(c.clientRouter, conn, []string{"queryops", aid, id}, c.logger)
	c.clientRouter.Register(client)

	go client.WritePump()
	go client.ReadPump()

	go func() {
		//TODO: make pubsub prefix aware...

		//
		// subscribe to account specific channel
		c.logger.With("channel", aid).Info("subscribe to client pubsub")
		c.clientPubsub.Subscribe(aid)
		//
		// subscribe to agent specific channel
		c.logger.With("channel", id).Info("subscribe to client pubsub")
		c.clientPubsub.Subscribe(id)
		//
		// tell client about all subscribed agents
		agents := []string{}
		for agent, _ := range c.agents[aid] {
			agents = append(agents, agent)
		}
		payload, err := json.Marshal(agents)
		if err != nil {
			c.logger.WithError(err).Warn("failed to marshal agents list")
		}
		envelope := messaging.Envelope{
			Recipients: []string{id},
			Message:    &messaging.Message{
				ID:        "0",
				From:      "",
				Subject:   "subscribed.agents",
				Payload:   payload,
				Timestamp: time.Now(),
			},
		}
		err = envelope.Send(context.Background(), c.clientPubsub)
		if err != nil {
			c.logger.WithError(err).Warn("failed to send agents list")
		}
		//
		// wait for client disconnect
		<-client.Closed
		//
		// delete from client tracking map
		delete(c.clients[aid], id)
		//
		// unsubscribe from agent specific channel
		c.logger.With("channel", aid).Info("unsubscribe from client pubsub")
		c.clientPubsub.Unsubscribe(aid)
		//
		// unsubscribe from account specific channel
		c.logger.With("channel", id).Info("unsubscribe from client pubsub")
		c.clientPubsub.Unsubscribe(id)
	}()
}

// onPubSubInbox get messages off the client pubsub and process them, i.e. send
// to appropriate clients
func (c *queryopsController) onClientPubsubInbox(ctx context.Context) {
	defer c.clientPubsub.Close()

	for {
		payload, err := c.clientPubsub.Receive(ctx)
		if err != nil {
			c.logger.WithError(err).Error("failed to receive message from client pubsub")
			continue
		}

		go c.processClientPubsubPayload(payload)
	}

	c.logger.Info("exited onClientPubsubInbox processing loop")
}

// processPubSubPayload send the envelope to the client router where
// it will deliver to the addressed clients
func (c *queryopsController) processClientPubsubPayload(payload []byte) {
	var envelope *messaging.Envelope

	if err := json.Unmarshal(payload, &envelope); err != nil {
		c.logger.WithError(err).Error("failed to deserialize envelope from client pubsub")
		return
	}

	c.clientRouter.Send(envelope)
}
