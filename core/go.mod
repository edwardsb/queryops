module gitlab.com/edwardsb/queryops/core

go 1.16

require (
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/cskr/pubsub v1.0.2
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.0
	github.com/go-chi/render v1.0.1
	github.com/go-redis/redis/v7 v7.4.0
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-multierror v1.0.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/osquery/osquery-go v0.0.0-20210607121309-df1df42c8206
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
)
