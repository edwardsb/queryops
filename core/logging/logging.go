package logging

// Fields used with WithFields
type Fields map[string]interface{}

// Level type
type Level uint8

const (
	// PanicLevel level, highest level of severity. Logs and then calls panic with the
	// message passed to Debug, Info, ...
	PanicLevel Level = iota
	// FatalLevel level. Logs and then calls `os.Exit(1)`. It will exit even if the
	// logging level is set to Panic.
	FatalLevel
	// ErrorLevel level. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	ErrorLevel
	// WarnLevel level. Non-critical entries that deserve eyes.
	WarnLevel
	// InfoLevel level. General operational entries about what's going on inside the
	// application.
	InfoLevel
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DebugLevel
)

type LogSetter interface {
	SetLogger(Interface, string)
}

func SetLogger(logger Interface, tag string) func(interface{}) {
	return func(i interface{}) {
		if l, ok := i.(LogSetter); ok {
			l.SetLogger(logger, tag)
		}
	}
}

// Interface is the interface that loggers need to implement
type Interface interface {
	// Level sets the log level to the specified level.  Any log request at or below that
	// level will be logged
	Level(level Level)

	Print(v ...interface{})
	Debug(v ...interface{})
	Info(v ...interface{})
	Warn(v ...interface{})
	Warning(args ...interface{})
	Error(v ...interface{})
	Panic(v ...interface{})
	Fatal(v ...interface{})

	Printf(format string, v ...interface{})
	Debugf(format string, v ...interface{})
	Infof(format string, v ...interface{})
	Warnf(format string, v ...interface{})
	Warningf(format string, args ...interface{})
	Errorf(format string, v ...interface{})
	Panicf(format string, v ...interface{})
	Fatalf(format string, v ...interface{})

	// WithTag adds a "tag" field to the log message
	WithTag(tag string) Interface
	// With adds supplied key/value pair 'key=value' to the log message
	With(key string, value interface{}) Interface
	WithField(key string, value interface{}) Interface
	// WithFields adds supplied key/value pairs to the log message
	WithFields(fields Fields) Interface
	// WithError adds an 'error=err' key/value pair to the log message
	WithError(err error) Interface

	Debugln(args ...interface{})
	Infoln(args ...interface{})
	Println(args ...interface{})
	Warnln(args ...interface{})
	Warningln(args ...interface{})
	Errorln(args ...interface{})
	Fatalln(args ...interface{})
	Panicln(args ...interface{})
}
