package logging

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

var _ Interface = (*logger)(nil)

// logger is used for logging
type logger struct {
	l *logrus.Entry
}

// New creates a new instance of Logger, ready to log
func New() Interface {
	l := logrus.New()

	_, awsRegionSet := os.LookupEnv("AWS_REGION")

	if awsRegionSet {
		l.Formatter = &logrus.JSONFormatter{
			TimestampFormat: time.RFC3339Nano,
			FieldMap: logrus.FieldMap{
				logrus.FieldKeyTime: "@timestamp",
				logrus.FieldKeyMsg:  "message",
			},
		}
	} else {
		l.Formatter = &logrus.TextFormatter{
			TimestampFormat: time.RFC3339Nano,
		}
	}

	l.Level = logrus.InfoLevel

	return &logger{
		l: logrus.NewEntry(l),
	}
}

func (l *logger) Level(level Level) {
	l.l.Logger.Level = logrus.Level(level)
}

func (l *logger) Print(v ...interface{}) {
	l.l.Print(v...)
}

func (l *logger) Printf(format string, args ...interface{}) {
	l.l.Printf(format, args...)
}

func (l *logger) Println(args ...interface{}) {
	l.l.Println(args...)
}

func (l *logger) Debug(v ...interface{}) {
	l.l.Debug(v...)
}

func (l *logger) Debugf(format string, args ...interface{}) {
	l.l.Debugf(format, args...)
}

func (l *logger) Debugln(args ...interface{}) {
	panic("implement me")
}

func (l *logger) Info(v ...interface{}) {
	l.l.Info(v...)
}

func (l *logger) Infof(format string, args ...interface{}) {
	l.l.Infof(format, args...)
}

func (l *logger) Infoln(args ...interface{}) {
	l.l.Infoln(args...)
}

func (l *logger) Warn(v ...interface{}) {
	l.l.Warn(v...)
}

func (l *logger) Warnf(format string, args ...interface{}) {
	l.l.Warnf(format, args...)
}

func (l *logger) Warnln(args ...interface{}) {
	l.l.Warnln(args...)
}

func (l *logger) Warning(args ...interface{}) {
	l.l.Warning(args...)
}

func (l *logger) Warningf(format string, args ...interface{}) {
	l.l.Warningf(format, args...)
}

func (l *logger) Warningln(args ...interface{}) {
	l.l.Warningln(args...)
}

func (l *logger) Error(v ...interface{}) {
	l.l.Error(v...)
}

func (l *logger) Errorf(format string, args ...interface{}) {
	l.l.Errorf(format, args...)
}

func (l *logger) Errorln(args ...interface{}) {
	l.l.Errorln(args...)
}

func (l *logger) Panic(v ...interface{}) {
	l.l.Panic(v...)
}

func (l *logger) Panicf(format string, args ...interface{}) {
	l.l.Panicf(format, args...)
}

func (l *logger) Panicln(args ...interface{}) {
	l.l.Panicln(args...)
}

func (l *logger) Fatal(v ...interface{}) {
	l.l.Fatal(v...)
}

func (l *logger) Fatalf(format string, args ...interface{}) {
	l.l.Fatalf(format, args...)
}

func (l *logger) Fatalln(args ...interface{}) {
	l.l.Fatalln(args...)
}

func (l *logger) WithTag(tag string) Interface {
	return l.With("tag", tag)
}

func (l *logger) WithFields(fields Fields) Interface {
	return &logger{
		l: l.l.WithFields(logrus.Fields(fields)),
	}
}

func (l *logger) WithField(key string, value interface{}) Interface {
	return l.With(key, value)
}

func (l *logger) With(key string, value interface{}) Interface {
	return &logger{
		l: l.with(key, value),
	}
}

func (l *logger) WithError(err error) Interface {
	return &logger{
		l: l.l.WithError(err),
	}
}

func (l *logger) with(key string, value interface{}) *logrus.Entry {
	return l.l.WithField(key, value)
}
