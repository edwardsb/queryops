package model

import (
	"fmt"
)

type Source struct {
	Pointer   string `json:"pointer,omitempty"`
	Parameter string `json:"parameter,omitempty"`
}

type APIError struct {
	Code   string                 `json:"code,omitempty"`
	Status string                 `json:"status,omitempty"`
	Title  string                 `json:"title,omitempty"`
	Detail string                 `json:"detail,omitempty"`
	Source *Source                `json:"source,omitempty"`
	Meta   map[string]interface{} `json:"meta,omitempty"`
	err    error
}

type APIErrors struct {
	Errors []*APIError `json:"errors"`
}

func (e *APIErrors) AddError(err *APIError) {
	e.Errors = append(e.Errors, err)
}

func NewError() *APIError {
	return &APIError{}
}

func NewErrorWithError(err error) *APIError {
	return &APIError{
		Title:  "error",
		Status: fmt.Sprintf("%d", HTTPStatusFromError(err)),
		Code:   fmt.Sprintf("0x%08X", ErrorCodeFromError(err)),
		Detail: err.Error(),
		Meta:   make(map[string]interface{}),
		err:    err,
	}
}

func (e *APIError) WithCode(code uint32) *APIError {
	e.Code = fmt.Sprintf("0x%08X", code)
	return e
}

func (e *APIError) WithStatus(status int) *APIError {
	e.Status = fmt.Sprintf("%d", status)
	return e
}

func (e *APIError) WithTitle(title string) *APIError {
	e.Title = title
	return e
}

func (e *APIError) WithDetail(detail string) *APIError {
	e.Detail = detail
	return e
}

func (e *APIError) WithSource(pointer, parameter string) *APIError {
	e.Source = &Source{
		Pointer:   pointer,
		Parameter: parameter,
	}
	return e
}

func (e *APIError) WithMeta(meta map[string]interface{}) *APIError {
	e.Meta = meta
	return e
}

func (e *APIError) Error() string {
	return e.err.Error()
}

func (e *APIError) ToErrors() *APIErrors {
	return &APIErrors{Errors: []*APIError{e}}
}
