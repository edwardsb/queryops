package model

import "net/http"

const ErrCodeSuccess uint32 = 0x00000000
const ErrCodeUnknown uint32 = 0xFFFFFFFF

type CoreError struct {
	error
	Code       uint32
	HttpStatus int
}


func NewCoreError(code uint32, status int, err error) error {
	return &CoreError{
		Code:       code,
		HttpStatus: status,
		error:      err,
	}
}

func (ce *CoreError) Error() string {
	return ce.error.Error()
}

func ErrorCodeFromError(err error) uint32 {
	c, ok := err.(*CoreError)
	if !ok {
		return ErrCodeUnknown
	}

	return c.Code
}

func HTTPStatusFromError(err error) int {
	c, ok := err.(*CoreError)
	if !ok {
		return http.StatusInternalServerError
	}

	return c.HttpStatus
}
