package api

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	http3 "gitlab.com/edwardsb/queryops/core/controller"
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules"
	http2 "gitlab.com/edwardsb/queryops/core/modules/http"
	"gitlab.com/edwardsb/queryops/core/modules/pubsub"
)

var _ modules.HTTPModule = (*websocketRouterV1)(nil)

type websocketRouterV1 struct {
	logger logging.Interface
	agentPubsub pubsub.Interface
	clientPubsub pubsub.Interface
}

func NewWebsocketRouterV1(options ...func(interface{})) modules.HTTPModule {
	m := &websocketRouterV1{
		logger: logging.New(),
		agentPubsub: pubsub.NewMemory(),
		clientPubsub:pubsub.NewMemory(),
	}

	for _, f := range options {
		if f == nil {
			continue
		}

		f(m)
	}

	return m
}

func SetAgentPubsub(pubsub pubsub.Interface) func(interface{}) {
	return func(i interface{}) {
		if w, ok := i.(*websocketRouterV1); ok {
			w.agentPubsub = pubsub
		}
	}
}

func SetClientPubsub(pubsub pubsub.Interface) func(interface{}) {
	return func(i interface{}) {
		if w, ok := i.(*websocketRouterV1); ok {
			w.clientPubsub = pubsub
		}
	}
}

func SetLogger(logger logging.Interface) func(interface{}) {
	return func(i interface{}) {
		if w, ok := i.(*websocketRouterV1); ok {
			w.logger = logger
		}
	}
}

func (w *websocketRouterV1) Pattern() string {
	return "/v1"
}

func (w *websocketRouterV1) SetupRouter() http.Handler {
	router := chi.NewRouter()

	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	router.Use(cors.Handler)
	router.Use(middleware.Logger)
	//
	// laod controllers
	ctrls := map[string]http2.Controller{}
	ctrls["websocket-controller-v1"] = http3.NewQueryOpsController(http3.SetAgentPubsub(w.agentPubsub), http3.SetClientPubsub(w.clientPubsub), http3.SetLogger(w.logger.WithTag("http.websocket-controller")))
	for k, ctrl := range ctrls {
		ctrl.SetRoutes(router)
		w.logger.With("controller", k).Info("loaded")
	}
	return router
}
