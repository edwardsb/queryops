package http

import (
	"net/http"
	"strings"

	"github.com/go-chi/render"
)

const (
	ContentTypeJSONAPI = render.ContentTypeEventStream + 1 + iota
)

// GetRequestContentType is a helper function that returns ContentType based on
// context or request headers.
func GetRequestContentType(r *http.Request) render.ContentType {
	if contentType, ok := r.Context().Value(render.ContentTypeCtxKey).(render.ContentType); ok {
		return contentType
	}
	return GetContentType(r.Header.Get("Content-Type"))
}

func GetContentType(s string) render.ContentType {
	s = strings.TrimSpace(strings.Split(s, ";")[0])
	switch s {
	default:
		return render.GetContentType(s)
	}
}

func DefaultDecoder(r *http.Request, v interface{}) error {
	switch GetRequestContentType(r) {
	default:
		return render.DefaultDecoder(r, v)
	}

}
