package http

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules"
)

var _ modules.Module = (*HTTP)(nil)

type HTTP struct {
	logger logging.Interface

	srv     *http.Server
	router  chi.Router
	config  *HTTPConfig
	modules []modules.HTTPModule
}

func Configuration(c *HTTPConfig) func(*HTTP) {
	return func(i *HTTP) {
		i.config = c
	}
}

func NewHTTP(httpModules []modules.HTTPModule, options ...func(*HTTP)) *HTTP {
	m := &HTTP{
		logger:  logging.New(),
		config:  DefaultHTTPConfig(),
		modules: httpModules,
	}

	for _, f := range options {
		f(m)
	}

	return m
}

func (m *HTTP) SetLogger(logger logging.Interface, tag string) {
	m.logger = logger.WithTag(tag)
}

func (m *HTTP) Open() error {
	if m.config.useSSL {
		if m.config.certKey == "" {
			err := errors.New("no SSL Certificate provided")
			m.logger.WithError(err).Error("refusing to load HTTP modules")

			return err
		}
		if m.config.certKey == "" {
			err := errors.New("no SSL Certificate key provided")
			m.logger.WithError(err).Error("refusing to load HTTP modules")

			return err
		}
	}

	m.initRouter()
	m.startHttpServer()

	return nil
}

func (m *HTTP) Close() error {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := m.srv.Shutdown(ctx)
	if err != nil {
		m.logger.WithError(err).Error("failed to gracefully shutdown http server")
		return err
	}

	return nil
}

func (m *HTTP) initRouter() {
	//
	// set the default decoder
	render.Decode = DefaultDecoder
	//
	// set the default responder
	render.Respond = DefaultResponder

	r := chi.NewRouter()
	// TODO: use metrics middleware
	//r.Use(middleware.Trace(), cmw.Compress(5), middleware.RequestID)
	r.Use(HealthCheck("/health"))
	//
	// debug route
	//r.Mount("/debug", cmw.Profiler())

	for _, module := range m.modules {
		r.Mount(module.Pattern(), module.SetupRouter())
	}

	m.router = r
}

func (m *HTTP) startHttpServer() {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", m.config.port),
		Handler: m.router,
	}

	go func() {
		if m.config.useSSL {
			err := srv.ListenAndServeTLS(m.config.certFile, m.config.certKey)
			if err != nil {
				m.logger.With("error", err).Fatalf("Failed to listen and startHttpServer")
			}
		} else {
			err := srv.ListenAndServe()
			if err != nil {
				m.logger.With("error", err).Fatalf("Failed to listen and startHttpServer")
			}
		}
	}()

	m.srv = srv

	m.logger.With("port", m.config.port).Info("http server started")
}

func HealthCheck(endpoint string) func(http.Handler) http.Handler {
	f := func(h http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			if (r.Method == "GET" || r.Method == "HEAD") && r.URL.Path == endpoint {
				w.WriteHeader(http.StatusOK)
				return
			}
			h.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
	return f
}
