package http


type HTTPConfig struct {
	useSSL   bool
	certFile string
	certKey  string
	port     int
}

func DefaultHTTPConfig() *HTTPConfig {
	return &HTTPConfig{
		useSSL:   false,
		certFile: "",
		certKey:  "",
		port:     4444,
	}
}

func NewHTTPConfig(useSSL bool, certFile string, certKey string, port int) *HTTPConfig {
	return &HTTPConfig{
		useSSL:   useSSL,
		certFile: certFile,
		certKey:  certKey,
		port:     port,
	}
}
