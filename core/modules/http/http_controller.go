package http

import "github.com/go-chi/chi"

type Controller interface {
	SetRoutes(router chi.Router)
}