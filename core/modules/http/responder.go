package http

import (
	"net/http"
	"strings"

	"github.com/go-chi/render"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/edwardsb/queryops/core/model"
)

func GetAcceptedContentType(r *http.Request) render.ContentType {
	if contentType, ok := r.Context().Value(render.ContentTypeCtxKey).(render.ContentType); ok {
		return contentType
	}

	var contentType render.ContentType

	// Parse request Accept header.
	fields := strings.Split(r.Header.Get("Accept"), ",")
	if len(fields) > 0 {
		contentType = GetContentType(strings.TrimSpace(fields[0]))
	}

	if contentType == render.ContentTypeUnknown {
		contentType = render.ContentTypePlainText
	}
	return contentType
}

// DefaultResponder handles streaming JSON and XML responses, automatically setting the
// Content-Type based on request headers. It will default to a JSON response.
func DefaultResponder(w http.ResponseWriter, r *http.Request, v interface{}) {
	if err, ok := v.(error); ok {
		errs := &model.APIErrors{}
		if merr, ok := err.(*multierror.Error); ok {
			for _, e := range merr.Errors {
				errs.AddError(model.NewErrorWithError(e))
			}
		} else {
			errs.AddError(model.NewErrorWithError(err))
		}
		render.Status(r, model.HTTPStatusFromError(err))
		render.JSON(w, r, errs)
		return
	}

	switch GetAcceptedContentType(r) {
	default:
		render.DefaultResponder(w, r, v)
	}
}