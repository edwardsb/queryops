package modules

import "net/http"

type HTTPModule interface {
	Pattern() string
	SetupRouter() http.Handler
}
