package messaging

import (
	"fmt"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/edwardsb/queryops/core/logging"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 20 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 1024
)

type Client struct {
	logger logging.Interface
	router *MessageRouter
	conn   *websocket.Conn
	rooms  []string
	outbox chan *Message
	Closed chan struct{}
}

func NewClient(router *MessageRouter, conn *websocket.Conn, rooms []string, logger logging.Interface) *Client {
	c := &Client{
		logger: logger.WithTag("messaging.client"),

		router: router,
		conn:   conn,
		rooms:  rooms,
		outbox: make(chan *Message, 100),
		Closed: make(chan struct{}),
	}

	return c
}

// readPump pumps messages from the websocket connection to the router.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) ReadPump() {
	defer func() {
		c.logger.With("client", fmt.Sprintf("%+v", c)).Info("closing socket for client")
		c.router.unregister <- c
		close(c.Closed)
		c.conn.Close()

	}()
	//c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		envelope := &Envelope{}
		err := c.conn.ReadJSON(envelope)
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure, websocket.CloseGoingAway) {
				c.logger.WithError(err).Error("websocket unexpectedly closed")
			}
			break
		}
		c.router.inbox <- envelope
	}
}

// writePump pumps messages from the router to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) WritePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		c.logger.Info("exit client write pump")
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.outbox:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.logger.Info("the router closed the channel")
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			err := c.conn.WriteJSON(message)
			if err != nil {
				c.logger.WithError(err).With("message", message).Warn("failed to write message")
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				c.logger.WithError(err).Warn("failed to write ping message")
				return
			}
		}
	}
}
