package messaging

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/hashicorp/go-multierror"
	"gitlab.com/edwardsb/queryops/core/modules/pubsub"
)

type Message struct {
	ID        string          `json:"id,omitempty"`      // ID
	From      string          `json:"from,omitempty"`    // sender
	Subject   string          `json:"subject,omitempty"` // subject
	Payload   json.RawMessage `json:"payload,omitempty"` // payload
	Timestamp time.Time       `json:"timestamp"`         // timestamp message sent
}

type Envelope struct {
	Recipients []string `json:"recipients,omitempty"`
	Message    *Message  `json:"message,omitempty"`
}

func (e *Envelope) Bind(r *http.Request) error {
	return nil
}

func (e *Envelope) Send(ctx context.Context, ps pubsub.Interface) error {
	buff, err := json.Marshal(e)
	if err != nil {
		return err
	}

	merr := &multierror.Error{}
	for _, recipient := range e.Recipients {
		err := ps.Publish(ctx, recipient, buff)
		if err != nil {
			merr.Errors = append(merr.Errors, err)
		}
	}

	return merr.ErrorOrNil()
}