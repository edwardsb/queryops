package messaging

import (
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules"
)

var _ modules.Module = (*MessageRouter)(nil)

type MessageRouter struct {
	logger logging.Interface
	// Registered clients.
	clients map[string]map[*Client]bool

	// Inbound messages from the clients.
	inbox chan *Envelope

	// Outbound messages for the clients.
	outbox chan *Envelope

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

func NewMessageRouter() *MessageRouter {
	return &MessageRouter{
		logger:     logging.New(),
		inbox:      make(chan *Envelope, 1024),
		outbox:     make(chan *Envelope, 1024),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[string]map[*Client]bool),
	}
}

func (r *MessageRouter) Open() error {
	go r.run()

	return nil
}

func (r *MessageRouter) Close() error {
	return nil
}

func (r *MessageRouter) Send(envelope *Envelope) {
	r.outbox <- envelope
}

func (r *MessageRouter) Receive() *Envelope {
	return <-r.inbox
}

func (r *MessageRouter) Register(client *Client) {
	r.register <- client
}

func (r *MessageRouter) run() {
	for {
		select {
		case client := <-r.register:
			for _, room := range client.rooms {
				_, ok := r.clients[room]
				if !ok {
					r.clients[room] = make(map[*Client]bool)
				}
				r.clients[room][client] = true
			}
		case client := <-r.unregister:
			for _, room := range client.rooms {
				r.logger.With("room", room).Info("unregister client from room")
				delete(r.clients[room], client)
			}
		case envelope := <-r.outbox:
			// deliver to the clients
			if len(envelope.Recipients) != 0 {
				for _, room := range envelope.Recipients {
					r.logger.With("target_room", room).With("targets", envelope.Recipients).Info("sending message to clients")
					for client := range r.clients[room] {
						client.outbox <- envelope.Message
					}
				}
			} else {
				// deliver to all the clients
				for _, clients := range r.clients {
					for client := range clients {
						client.outbox <- envelope.Message
					}
				}
			}
		}
	}
}
