package modules

type Module interface {
	Open() error
	Close() error
}