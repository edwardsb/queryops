package pubsub

import (
	"context"

	ps "github.com/cskr/pubsub"
	"gitlab.com/edwardsb/queryops/core/logging"
	"gitlab.com/edwardsb/queryops/core/modules"
)

var _ Interface = (*memoryPubSub)(nil)
var _ modules.Module = (*memoryPubSub)(nil)

type memoryPubSub struct {
	logger logging.Interface
	pubsub *ps.PubSub
	rx     chan interface{}
	prefix string
}

// NewMemory creates a new pubsub using process memory
func NewMemory(options ...func(interface{})) Interface {
	ps := &memoryPubSub{
		logger: logging.New(),
		pubsub: ps.New(10000),
	}

	for _, f := range options {
		if f == nil {
			continue
		}

		f(ps)
	}

	return ps
}

func (ps *memoryPubSub) Prefix(prefix string) {
	ps.prefix = prefix
}

func (ps *memoryPubSub) Open() error {
	return nil
}

func (ps *memoryPubSub) Close() error {
	ps.pubsub.Shutdown()
	return nil
}

func (ps *memoryPubSub) Subscribe(channels ...string) error {
	if len(ps.prefix) > 0 {
		channels = Map(channels, func(s string) string {
			return ps.prefix + s
		})
	}

	if ps.rx == nil {
		ps.rx = ps.pubsub.Sub(channels...)
		return nil
	}

	ps.pubsub.AddSub(ps.rx, channels...)
	return nil
}

func (ps *memoryPubSub) Unsubscribe(channels ...string) error {
	if len(ps.prefix) > 0 {
		channels = Map(channels, func(s string) string {
			return ps.prefix + s
		})
	}

	ps.pubsub.Unsub(ps.rx, channels...)
	return nil
}

func (ps *memoryPubSub) Receive(ctx context.Context) ([]byte, error) {
	msg, ok := <-ps.rx
	if !ok {
		panic("PubSub receive channel closed")
	}

	data := msg.([]byte)

	return data, nil
}

func (ps *memoryPubSub) Publish(ctx context.Context, channel string, message []byte) error {
	ps.logger.With("channel", channel).Info("publishing to channel")

	if len(ps.prefix) > 0 {
		channel = ps.prefix + channel
	}
	ps.pubsub.Pub(message, channel)
	return nil
}
