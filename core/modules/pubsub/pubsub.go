package pubsub

import (
	"context"
	"gitlab.com/edwardsb/queryops/core/modules"
)

// Interface is used by SocketHandler to receive Inbox messages
type Interface interface {
	modules.Module

	// Publish to the pubsub channel
	Publish(ctx context.Context, channel string, payload []byte) error

	// Receive from the pubsub channel
	Receive(context.Context) ([]byte, error)

	Subscribe(channels ...string) error

	Unsubscribe(channels ...string) error
}

func Subscribe(channel string) func(interface{}) {
	return func(i interface{}) {
		if iface, ok := i.(Interface); ok {
			_ = iface.Subscribe(channel)
		}
	}
}

type prefixable interface {
	Prefix(prefix string)
}

func SetChannelPrefix(prefix string) func(interface{}) {
	return func(i interface{}) {
		if ps, ok := i.(prefixable); ok && len(prefix) > 0 {
			ps.Prefix(prefix)
		}
	}
}

func Map(vs []string, f func(string) string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}