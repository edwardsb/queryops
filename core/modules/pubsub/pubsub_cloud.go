// +build cloud

package pubsub

func NewPubsub(options ...func(interface{})) Interface {
	return NewRedisPubsub(options...)
}