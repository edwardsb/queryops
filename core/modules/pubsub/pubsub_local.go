// +build local !cloud

package pubsub

func NewPubsub(options ...func(interface{})) Interface {
	return NewMemory(options...)
}