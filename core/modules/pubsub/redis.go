package pubsub

import (
	"context"
	"sync"

	"github.com/go-redis/redis/v7"
	"gitlab.com/edwardsb/queryops/core/modules"
)

var _ Interface = (*redisPubSub)(nil)
var _ modules.Module = (*redisPubSub)(nil)

// RedisPubSub implements PubSub for a redis client
type redisPubSub struct {
	client *redis.Client
	pubsub *redis.PubSub
	prefix string
	mux sync.Mutex
}

// NewRedisPubsub creates a PubSub for use with redis.
// Pass in a redis client and a channel name and this will subscribe to the specified channel,
// allow retrieving messages from the channel, and allow publishing to the channel.
// This will manage closing the client.
func NewRedisPubsub(options ...func(interface{})) Interface {
	ps := &redisPubSub{
		client: redis.NewClient(&redis.Options{
			Addr: "localhost:6379",
		}),
	}

	for _, f := range options {
		if f == nil {
			continue
		}

		f(ps)
	}
	return ps
}

func SetClient(client *redis.Client) func(interface{}) {
	return func(i interface{}) {
		if ps, ok := i.(*redisPubSub); ok {
			ps.client = client
		}
	}
}

func (ps *redisPubSub) Prefix(prefix string) {
	ps.prefix = prefix
}

func (ps *redisPubSub) Open() error {
	return nil
}

// Close the pubsub channel and redis client
func (ps *redisPubSub) Close() error {
	if ps.pubsub != nil {
		if err := ps.pubsub.Close(); err != nil {
			return err
		}
	}

	return nil
}

func (ps *redisPubSub) Subscribe(channels ...string) error {
	if len(ps.prefix) > 0 {
		channels = Map(channels, func(s string) string {
			return ps.prefix + s
		})
	}

	if ps.pubsub == nil {
		ps.pubsub = ps.client.Subscribe(channels...)

		return nil
	}

	return ps.pubsub.Subscribe(channels...)
}

func (ps *redisPubSub) Unsubscribe(channels ...string) error {
	if len(ps.prefix) > 0 {
		channels = Map(channels, func(s string) string {
			return ps.prefix + s
		})
	}
	return ps.pubsub.Unsubscribe(channels...)
}

// Publish to the pubsub channel
func (ps *redisPubSub) Publish(ctx context.Context, channel string, data []byte) error {
	if len(ps.prefix) > 0 {
		channel = ps.prefix + channel
	}
	err := ps.client.WithContext(ctx).Publish(channel, string(data)).Err()
	if err != nil {
		return err
	}

	return nil
}

// Receive from the pubsub channel
func (ps *redisPubSub) Receive(ctx context.Context) ([]byte, error) {
	ps.mux.Lock()
	defer ps.mux.Unlock()

	msg, err := ps.pubsub.ReceiveMessage()
	if err != nil {
		return nil, err
	}

	if msg == nil {
		return nil, nil
	}

	return []byte(msg.Payload), nil
}
