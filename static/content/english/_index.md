---
####################### Banner #########################
banner:
  title : "Query all the things"
  image : "images/banner-art.svg"
  content : "In today's remote and distributed workforce it's important to be able to know what your managed endpoints are doing. QueryOps gives you the power to query endpoints and get the details."
  button:
    enable : false
    label : "Contact With Us"
    link : "contact"

##################### Feature ##########################
feature:
  enable : true
  title : "Features"
  feature_item:
    # feature item loop
    - name : "Live Query"
      icon : "fas fa-code"
      content : "Get realtime query results from your endpoints"

    # feature item loop
    - name : "Faster Response"
      icon : "fas fa-tachometer-alt"
      content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      
    # feature item loop
    - name : "Cloud Native"
      icon : "fas fa-cloud"
      content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      


######################### Service #####################
service:
  enable : true
  service_item:
    # service item loop
    - title : "Designed for scale and cloud native"
      images:
      - "images/service-1.png"
      - "images/service-2.png"
      - "images/service-3.png"
      content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat. consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
    # service item loop
    - title : "Team of experience cybersecurity engineers"
      images:
      - "images/service-2.png"
      content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat. consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
################### Screenshot ########################
screenshot:
  enable : false
  title : "Experience the best <br> workflow with us"
  image : "images/screenshot.svg"

  

##################### Call to action #####################
call_to_action:
  enable : true
  title : "Ready to get started?"
  image : "images/cta.svg"
  content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur."
  button:
    enable : true
    label : "Contact Us"
    link : "contact"
---