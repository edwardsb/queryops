---
title: "Pricing"
subtitle: ""
# meta description
description: "Money n stuff"
draft: false

basic:
  name : "Free Plan"
  price: "$0"
  price_per : "host"
  info : "Try it yourself"
  services:
  - "Live Query"
  button:
    enable : true
    label : "Get started for free"
    link : "https://app.queryops.com"
    
professional:
  name : "Professional Plan"
  price: "$5"
  price_per : "host"
  info : "Best For Professionals"
  services:
  - "Live Query"
  - "Scheduled Queries"
  - "Historical Search"
  button:
    enable : true
    label : "Get started for free"
    link : "#"

call_to_action:
  enable : true
  title : "Need a larger plan?"
  image : "images/cta.svg"
  content : "Get in touch and lets discuss your needs."
  button:
    enable : true
    label : "Contact Us"
    link : "contact"
---